core = 7.x
api = 2

;------------------------------------------------------------------------------------------------------------------------
; Contrib
; Contributed modules from Drupal.org
;------------------------------------------------------------------------------------------------------------------------

; Biblio
projects[biblio][type] = "module"
projects[biblio][download][type] = "git"
projects[biblio][download][url] = "https://git.uwaterloo.ca/drupal-org/biblio.git"
projects[biblio][download][branch] = "7.x-1.0-rc8-uw_wcms"
projects[biblio][download][revision] = "ca4ca98"
projects[biblio][subdir] = "contrib"

; CAS
projects[cas][type] = "module"
projects[cas][download][type] = "git"
projects[cas][download][url] = "https://git.uwaterloo.ca/drupal-org/cas.git"
projects[cas][download][tag] = "7.x-1.5"
projects[cas][subdir] = "contrib"

; Conditional Fields
; Base: 7.x-3.0-alpha2
; Patch efd3840: Fix undefined index error on feature enable. https://www.drupal.org/node/2043563#comment-9049737
projects[conditional_fields][type] = "module"
projects[conditional_fields][download][type] = "git"
projects[conditional_fields][download][url] = "https://git.uwaterloo.ca/drupal-org/conditional_fields.git"
projects[conditional_fields][download][tag] = "7.x-3.0-alpha2-uw_wcms1"
projects[conditional_fields][subdir] = "contrib"

; CKEditor Link
projects[ckeditor_link][type] = "module"
projects[ckeditor_link][download][type] = "git"
projects[ckeditor_link][download][url] = "https://git.uwaterloo.ca/drupal-org/ckeditor_link.git"
projects[ckeditor_link][download][branch] = "7.x-2.4-uw_os"
projects[ckeditor_link][download][revision] = "676587d"
projects[ckeditor_link][subdir] = "contrib"

; EIM
projects[eim][type] = "module"
projects[eim][download][type] = "git"
projects[eim][download][url] = "https://git.uwaterloo.ca/drupal-org/eim.git"
projects[eim][download][tag] = "7.x-1.3-uw_wcms2"
projects[eim][subdir] = "contrib"

; Features
; Base 7.x-2.10
; Patch 5e6952b: Give site maintainers the option to disable content types. https://www.drupal.org/node/2855810#comment-11956286
projects[features][type] = "module"
projects[features][download][type] = "git"
projects[features][download][url] = "https://git.uwaterloo.ca/drupal-org/features.git"
projects[features][download][tag] = "7.x-2.10-uw_wcms2"
projects[features][subdir] = "contrib"

; Features Override
projects[features_override][type] = "module"
projects[features_override][download][type] = "git"
projects[features_override][download][url] = "https://git.uwaterloo.ca/drupal-org/features_override.git"
projects[features_override][download][tag] = "7.x-2.0-rc3"
projects[features_override][subdir] = "contrib"

; Field Collection
; Base: 280de24 (7.x-1.0-beta12+4-dev)
projects[field_collection][type] = "module"
projects[field_collection][download][type] = "git"
projects[field_collection][download][url] = "https://git.uwaterloo.ca/drupal-org/field_collection.git"
projects[field_collection][download][tag] = "7.x-1.0-beta12+4-dev-uw_wcms1"
projects[field_collection][subdir] = "contrib"

; Focal Point
; Base: 6d36945 (7.x-1.0-beta4+4-dev)
; Patch 1363913: Added settings per fields vs. enabled for all image fields. Related to: https://www.drupal.org/node/2480947
projects[focal_point][type] = "module"
projects[focal_point][download][type] = "git"
projects[focal_point][download][url] = "https://git.uwaterloo.ca/drupal-org/focal_point.git"
projects[focal_point][download][tag] = "7.x-1.0-beta4-uw_wcms1"
projects[focal_point][subdir] = "contrib"

; jcarousel
projects[jcarousel][type] = "module"
projects[jcarousel][download][type] = "git"
projects[jcarousel][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/jcarousel.git"
projects[jcarousel][download][tag] = "7.x-2.7"
projects[jcarousel][subdir] = "contrib"

; jquery update
projects[jquery_update][type] = "module"
projects[jquery_update][download][type] = "git"
projects[jquery_update][download][url] = "https://git.uwaterloo.ca/drupal-org/jquery_update.git"
projects[jquery_update][download][tag] = "7.x-3.0-alpha5"
projects[jquery_update][subdir] = "contrib"

; Image Field Caption
projects[image_field_caption][type] = "module"
projects[image_field_caption][download][type] = "git"
projects[image_field_caption][download][url] = "https://git.uwaterloo.ca/drupal-org/image_field_caption.git"
projects[image_field_caption][download][branch] = "7.x-2.x"
projects[image_field_caption][download][revision] = "07780b34"
projects[image_field_caption][subdir] = "contrib"

; Mathjax
projects[mathjax][type] = "module"
projects[mathjax][download][type] = "git"
projects[mathjax][download][url] = "https://git.uwaterloo.ca/drupal-org/mathjax.git"
projects[mathjax][download][tag] = "7.x-2.5"
projects[mathjax][subdir] = "contrib"

; Module filter
; base: 7.x-2.0
; Patch 91779bf4: #1969856 Incompatibility with Tab mode and Coder Review module. https://www.drupal.org/node/1969856
projects[module_filter][type] = "module"
projects[module_filter][download][type] = "git"
projects[module_filter][download][url] = "https://git.uwaterloo.ca/drupal-org/module_filter.git"
projects[module_filter][download][tag] = "7.x-2.0-uw_wcms1"
projects[module_filter][subdir] = "contrib"

; Real name
projects[realname][type] = "module"
projects[realname][download][type] = "git"
projects[realname][download][url] = "https://git.uwaterloo.ca/drupal-org/realname.git"
projects[realname][download][tag] = "7.x-1.2"
projects[realname][subdir] = "contrib"

; RoleAssign
projects[roleassign][type] = "module"
projects[roleassign][download][type] = "git"
projects[roleassign][download][url] = "https://git.uwaterloo.ca/drupal-org/roleassign.git"
projects[roleassign][download][tag] = "7.x-1.0"
projects[roleassign][subdir] = "contrib"

; Role Expire
; Base: 7.x-1.0-beta2
; Patch c4b5461: Role Assign integration. https://drupal.org/comment/8706305#comment-8706305
projects[role_expire][type] = "module"
projects[role_expire][download][type] = "git"
projects[role_expire][download][url] = "https://git.uwaterloo.ca/drupal-org/role_expire.git"
projects[role_expire][download][tag] = "7.x-1.0-beta2-uw_wcms1"
projects[role_expire][subdir] = "contrib"

;------------------------------------------------------------------------------------------------------------------------
; Custom Modules
; These modules are developed in-house
;------------------------------------------------------------------------------------------------------------------------

; uWaterloo CAS username
projects[uw_cas_username][type] = "module"
projects[uw_cas_username][download][type] = "git"
projects[uw_cas_username][download][url] = "https://git.uwaterloo.ca/wcms/uw_cas_username.git"
projects[uw_cas_username][download][tag] = "7.x-1.1"
projects[uw_cas_username][subdir] = "custom"

; uWaterloo LDAP
projects[uw_ldap][type] = "module"
projects[uw_ldap][download][type] = "git"
projects[uw_ldap][download][url] = "https://git.uwaterloo.ca/wcms/uw_ldap.git"
projects[uw_ldap][download][tag] = "7.x-1.12"
projects[uw_ldap][subdir] = "custom"

;------------------------------------------------------------------------------------------------------------------------
; Development Modules
; These modules are used primarily by developers
;------------------------------------------------------------------------------------------------------------------------

; Assign WCMS Groups from Active Directory
projects[uw_auth_wcms_admins][type] = "module"
projects[uw_auth_wcms_admins][download][type] = "git"
projects[uw_auth_wcms_admins][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_wcms_admins.git"
projects[uw_auth_wcms_admins][download][tag] = "7.x-1.15"
projects[uw_auth_wcms_admins][subdir] = "dev"

;------------------------------------------------------------------------------------------------------------------------
; Features
; Modules that store config information in code instead of the database
;------------------------------------------------------------------------------------------------------------------------

; CAS Authentication Common
projects[uw_auth_cas_common][type] = "module"
projects[uw_auth_cas_common][download][type] = "git"
projects[uw_auth_cas_common][download][url] = "https://git.uwaterloo.ca/wcms/uw_auth_cas_common.git"
projects[uw_auth_cas_common][download][tag] = "7.x-1.9"
projects[uw_auth_cas_common][subdir] = "features"

; FDSU Roles
projects[uw_roles_fdsu][type] = "module"
projects[uw_roles_fdsu][download][type] = "git"
projects[uw_roles_fdsu][download][url] = "https://git.uwaterloo.ca/wcms/uw_roles_fdsu.git"
projects[uw_roles_fdsu][download][tag] = "7.x-1.13"
projects[uw_roles_fdsu][subdir] = "features"

; ORCID Integration
projects[os_orcid][type] = "module"
projects[os_orcid][download][type] = "git"
projects[os_orcid][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/os_orcid.git"
projects[os_orcid][download][tag] = "7.x-1.3"
projects[os_orcid][subdir] = "features"

; UW Configure Open Scholar
projects[uw_cfg_openscholar][type] = "module"
projects[uw_cfg_openscholar][download][type] = "git"
projects[uw_cfg_openscholar][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_cfg_openscholar.git"
projects[uw_cfg_openscholar][download][tag] = "7.x-1.7"
projects[uw_cfg_openscholar][subdir] = "features"

; UW-OS Responsive Menu
projects[uw_os_responsive_menu][type] = "module"
projects[uw_os_responsive_menu][download][type] = "git"
projects[uw_os_responsive_menu][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_os_responsive_menu.git"
projects[uw_os_responsive_menu][download][tag] = "7.x-1.1"
projects[uw_os_responsive_menu][subdir] = "features"

; UW Virtual Sites Homepage
projects[uw_virtual_site_homepage][type] = "module"
projects[uw_virtual_site_homepage][download][type] = "git"
projects[uw_virtual_site_homepage][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_virtual_site_homepage.git"
projects[uw_virtual_site_homepage][download][tag] = "7.x-1.6"
projects[uw_virtual_site_homepage][subdir] = "features"

; Vsite citation style
projects[vsite_citation_style][type] = "module"
projects[vsite_citation_style][download][type] = "git"
projects[vsite_citation_style][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/vsite_citation_style.git"
projects[vsite_citation_style][download][tag] = "7.x-1.1"
projects[vsite_citation_style][subdir] = "openscholar"

;------------------------------------------------------------------------------------------------------------------------
; Libraries
;------------------------------------------------------------------------------------------------------------------------

; phpCAS
; https://github.com/Jasig/phpCAS
libraries[CAS][download][type] = "git"
libraries[CAS][download][url] = "https://git.uwaterloo.ca/libraries/phpcas.git"
libraries[CAS][download][tag] = "1.3.3"
libraries[CAS][directory_name] = "CAS"

; autopager
libraries[autopager][download][type] = "git"
libraries[autopager][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/autopager.git"
libraries[autopager][download][tag] = "1.0.0-uw_wcms1"
libraries[autopager][directory_name] = "autopager"

;------------------------------------------------------------------------------------------------------------------------
; Themes
;------------------------------------------------------------------------------------------------------------------------

; uWaterloo base theme for Open Scholar
projects[uw_basetheme][type] = "theme"
projects[uw_basetheme][download][type] = "git"
projects[uw_basetheme][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_basetheme.git"
projects[uw_basetheme][download][tag] = "7.x-1.2"

; uWaterloo Open Scholar base theme
projects[uw_os_basetheme][type] = "theme"
projects[uw_os_basetheme][download][type] = "git"
projects[uw_os_basetheme][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_os_basetheme.git"
projects[uw_os_basetheme][download][tag] = "7.x-1.8"

; uWaterloo theme for Personal (Virtual) sites in Open Scholar
projects[uw_personal_site_theme][type] = "theme"
projects[uw_personal_site_theme][download][type] = "git"
projects[uw_personal_site_theme][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_personal_site_theme.git"
projects[uw_personal_site_theme][download][tag] = "7.x-1.3"

; Adminimal theme
projects[adminimal_theme][type] = "theme"
projects[adminimal_theme][download][type] = "git"
projects[adminimal_theme][download][url] = "https://git.uwaterloo.ca/drupal-org/adminimal_theme.git"
projects[adminimal_theme][download][tag] = "7.x-1.26"

; UWaterloo-OpenScholar Adminimal theme
projects[uw_os_adminimal_theme][type] = "theme"
projects[uw_os_adminimal_theme][download][type] = "git"
projects[uw_os_adminimal_theme][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/uw_os_adminimal_theme.git"
projects[uw_os_adminimal_theme][download][tag] = "7.x-1.1"

;------------------------------------------------------------------------------------------------------------------------
; Modded Open Scholar Modules
; These modules are developed on top of the Open Scholar modules.  These should eventually be looked at and removed
; if possible.
;------------------------------------------------------------------------------------------------------------------------

; CP Toolbar
projects[cp_toolbar][type] = "module"
projects[cp_toolbar][download][type] = "git"
projects[cp_toolbar][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/cp_toolbar.git"
projects[cp_toolbar][download][tag] = "7.x-1.1"
projects[cp_toolbar][subdir] = "openscholar"

; Nice Menus
projects[nice_menus][type] = "module"
projects[nice_menus][download][type] = "git"
projects[nice_menus][download][url] = "https://git.uwaterloo.ca/wcms-openscholar/nice_menus.git"
projects[nice_menus][download][tag] = "7.x-1.1"
projects[nice_menus][subdir] = "openscholar"
